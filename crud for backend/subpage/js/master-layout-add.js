$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
        var gUrl = "http://localhost:8080/master_layouts";
        var gProjectUrl = "http://localhost:8080/projects"
    
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
        onPageLoading();
    
        $(document).on("click", "#btn-add", function(){
            onBtnAddClick();
        })
    
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
        function onPageLoading(){
            "use strict"
            callApiGetProjects();
        }
    
        //Hàm xử lí sự kiện nhất nút cập nhật
        function onBtnAddClick(){
            "use strict"
            // khai báo dữ liệu cập nhật
            var vDataUpdate = {
                name: "",
                acreage: 0,
                apartmentList: "",
                description: "",
                photo: "",
            }
            var vIdProject = $("#select-project :selected").val();
            //B1: Thu thập dữ liệu
            readDataForm(vDataUpdate);
            console.log(vDataUpdate);
            console.log(vIdProject);
            //B2: Kiểm tra dữ liệu(bỏ qua)
            //B3: Cập nhật dữ liệu
            callApiAddMasterLayout(vDataUpdate, vIdProject);
    
        }
        // Hàm gọi api add master layout
        function callApiAddMasterLayout(paramObj, paramId) {
            "use strict";
            console.log("abc " + paramId);
            var vJsonUpdateData = JSON.stringify(paramObj);
            $.ajax({
                url: gUrl + "/" + paramId,
                type: "POST",
                contentType: "application/json",
                data: vJsonUpdateData,
                success: function (res) {
                    console.log(res);
                    alert("successfully!")
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to create!");
                },
            });
        }

        // Hàm gọi lấy tất cả project
        function callApiGetProjects(){
            "use strict";
            $.ajax({
                url: gProjectUrl,
                type: "GET",
                contentType: "application/json",
                success: function (res) {
                    console.log(res);
                    loadProjectToSelectBox(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
        // Hàm thu thập dữ liệu
        function readDataForm(paramObj){
            "use strict"
            paramObj.name = $("#inp-name").val();
            paramObj.acreage = $("#inp-acreage").val();
            paramObj.apartmentList = $("#inp-apartment-list").val();
            paramObj.description = $("#inp-description").val();
            paramObj.photo = $("#inp-photo").val();
        }

        // Hàm tải dữ liệu project lên form
        function loadProjectToSelectBox(paramArr){
            "use strict"
            var vSelectBox = $("#select-project");
            for(var bI = 0; bI < paramArr.length; bI ++){
                var bOption = $("<option>");
                bOption.prop("value", paramArr[bI].id);
                bOption.prop("text", paramArr[bI].name);
                vSelectBox.append(bOption);
            }
        }
    });