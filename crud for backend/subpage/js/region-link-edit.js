$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
        var gUrl = new URL(window.location.href);
    
        var gId = gUrl.searchParams.get("id");
    
        var gUrl = "http://localhost:8080/region_links";
    
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
        onPageLoading();
    
        $(document).on("click", "#btn-update", function(){
            onBtnUpdateClick();
        })
    
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
        function onPageLoading(){
            "use strict"
            callApiGetRegionLinkById(gId);
        }
    
        //Hàm xử lí sự kiện nhất nút cập nhật
        function onBtnUpdateClick(){
            "use strict"
            // khai báo dữ liệu cập nhật
            var vDataUpdate = {
                name:"",
                address: "",
                description: "",
                photo: "",
                lat: "",
                lng: "",

            }
            //B1: Thu thập dữ liệu
            readDataForm(vDataUpdate);
            console.log(vDataUpdate);
            //B2: Kiểm tra dữ liệu(bỏ qua)
            //B3: Cập nhật dữ liệu
            callApiUpdateRegionLink(vDataUpdate);
    
        }
        // Hàm gọi api lấy address map by id
        function callApiGetRegionLinkById(paramId) {
            "use strict";
            $.ajax({
                url: gUrl + "/" + paramId,
                type: "GET",
                dataType: "json",
                success: function (res) {
                    console.log(res);
                    loadDataToForm(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get investor!");
                },
            });
        }
        // Hàm gọi api cập nhật address map by id
        function callApiUpdateRegionLink(paramObj) {
            "use strict";
            var vJsonUpdateData = JSON.stringify(paramObj);
            $.ajax({
                url: gUrl + "/" + gId,
                type: "PUT",
                contentType: "application/json",
                data: vJsonUpdateData,
                success: function (res) {
                    console.log(res);
                    alert("successfully!")
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to update!");
                },
            });
        }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
        // Hàm tải dữ liệu vào form
        function loadDataToForm(paramObj){
            "use strict"
            $("#inp-id").val(paramObj.id);
            $("#inp-name").val(paramObj.name);
            $("#inp-address").val(paramObj.address);
            $("#inp-description").val(paramObj.description);
            $("#inp-photo").val(paramObj.photo);
            $("#inp-lat").val(paramObj.lat);
            $("#inp-lng").val(paramObj.lng);
        }
    
        // Hàm thu thập dữ liệu
        function readDataForm(paramObj){
            "use strict"
            paramObj.name = $("#inp-name").val();
            paramObj.address = $("#inp-address").val();
            paramObj.description = $("#inp-description").val();
            paramObj.photo = $("#inp-photo").val();
            paramObj.lat = $("#inp-lat").val();
            paramObj.lng = $("#inp-lng").val();
        }
    });