package com.devcamp.s50.provincedistrictwardapi.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.s50.provincedistrictwardapi.model.CCustomer;
import com.devcamp.s50.provincedistrictwardapi.repository.ICustomerRepository;

@Service
public class CCustomerService {
    @Autowired
    ICustomerRepository customerRepository;
    // Get all Customers
    public ResponseEntity<List<CCustomer>> getAllCustomers(String page, String size){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<CCustomer> customerList = new ArrayList<>();
            customerRepository.findAll(pageWithElements).forEach(customerList::add);

            return new ResponseEntity<>(customerList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // Get customer by id
    public ResponseEntity<Object> getCustomerById (int id){
        try {
            Optional<CCustomer> customerData = customerRepository.findById(id);
            if(customerData.isPresent()){
                CCustomer customerFound = customerData.get();

                return new ResponseEntity<>(customerFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Create a new customer
    public ResponseEntity<Object> createCustomer(CCustomer pCustomer){
        try {
            CCustomer newCustomer = new CCustomer();

                newCustomer.setContactName(pCustomer.getContactName());
                newCustomer.setContactTitle(pCustomer.getContactTitle());
                newCustomer.setAddress(pCustomer.getAddress());
                newCustomer.setMobile(pCustomer.getMobile());
                newCustomer.setEmail(pCustomer.getEmail());
                newCustomer.setNote(pCustomer.getNote());
                newCustomer.setCreateBy(pCustomer.getCreateBy());
                newCustomer.setCreateDate(new Date());

                CCustomer savedCustomer = customerRepository.save(newCustomer);

                return new ResponseEntity<>(savedCustomer, HttpStatus.CREATED);

        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity().body("Failed to Create a specified customer: " + e.getCause().getCause().getMessage());
        }
    }

    // update a new customer
    public ResponseEntity<Object> updateteCustomer(int id, CCustomer pCustomer){
        try {
            Optional<CCustomer> customerData = customerRepository.findById(id);
            if (customerData.isPresent()){

                CCustomer customerFound = customerData.get();

                customerFound.setContactName(pCustomer.getContactName());
                customerFound.setContactTitle(pCustomer.getContactTitle());
                customerFound.setAddress(pCustomer.getAddress());
                customerFound.setMobile(pCustomer.getMobile());
                customerFound.setEmail(pCustomer.getEmail());
                customerFound.setNote(pCustomer.getNote());
                customerFound.setUpdateBy(pCustomer.getUpdateBy());
                customerFound.setUpdateDate(new Date());

                CCustomer savedCustomer = customerRepository.save(customerFound);

                return new ResponseEntity<>(savedCustomer, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // delete a customer
    public ResponseEntity<Object> deleteCustomerById(int id){
        try {
            Optional<CCustomer> customerData = customerRepository.findById(id);
            if(customerData.isPresent()){
                customerRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
