package com.devcamp.s50.provincedistrictwardapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.provincedistrictwardapi.model.CRegionLink;
import com.devcamp.s50.provincedistrictwardapi.service.CRegionLinkService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CRegionLinkController {
    @Autowired
    CRegionLinkService regionLinkService;

    //Get all region links
    @GetMapping("region_links")
    public ResponseEntity<List<CRegionLink>> getAllRegionLinks(
        @RequestParam(defaultValue = "0") String page,
        @RequestParam(defaultValue = "50") String size){
        return regionLinkService.getAllRegionLinks(page, size);
    }

    //Get Region link by Id;
    @GetMapping("region_links/{id}")
    public ResponseEntity<Object> getRegionLinkById (@PathVariable Integer id){
        return regionLinkService.getRegionLinkById(id);
    }

    // Create a Region Link
    @PostMapping("region_links")
    public ResponseEntity<Object> createRegionLink(@RequestBody CRegionLink pRegionLink){
        return regionLinkService.createRegionLink(pRegionLink);
    }

    // Update Region link by id
    @PutMapping("region_links/{id}")
    public ResponseEntity<Object> updateDesignUnitById(@PathVariable Integer id,@RequestBody CRegionLink pRegionLink){
        return regionLinkService.updateDesignUnitById(id, pRegionLink);
    }

    //delete region link by id
    @DeleteMapping("region_links/{id}")
    public ResponseEntity<Object> deleteRegionLinkById(@PathVariable Integer id){
        return regionLinkService.deleteRegionLinkById(id);
    }
}
