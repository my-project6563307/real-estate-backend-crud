package com.devcamp.s50.provincedistrictwardapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.s50.provincedistrictwardapi.model.CRealEstate;

public interface IRealEstateRepository extends JpaRepository<CRealEstate, Integer>{
@Query(value = "select * from realestate where request = :request", nativeQuery = true)
List<CRealEstate> findByRequest(@Param("request") String request);
}
