package com.devcamp.s50.provincedistrictwardapi.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.provincedistrictwardapi.model.CWard;
import com.devcamp.s50.provincedistrictwardapi.service.CWardService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CWardController {
    @Autowired
    CWardService wardService;
    
    @GetMapping("wards")
    public ResponseEntity<List<CWard>> getAllWards(
    @RequestParam(defaultValue = "0") String page,
    @RequestParam(defaultValue = "50") String size){
        return wardService.getAllWards(page, size);
    }
    @GetMapping("wards/{id}")
    public ResponseEntity<Object> getWardById(@PathVariable int id){
        return wardService.getWardById(id);
    }

    @GetMapping("wards/district/{districtId}")
    public ResponseEntity<Set<CWard>> getWardByDistrictId(@PathVariable int districtId){
        return wardService.getWardByDistrictId(districtId);
    }

    @PostMapping("wards/province/{provinceId}/district/{districtId}")
    public ResponseEntity<Object> createWard(@PathVariable int provinceId,@PathVariable int districtId,@RequestBody CWard pWard){
        return wardService.createWard(provinceId, districtId, pWard);
    }

    @PutMapping("wards/{id}")
    public ResponseEntity<Object> updateWardById(@RequestParam int provinceId,@RequestParam int districtId, @PathVariable int id,@RequestBody CWard pWard){
        return wardService.updateWardById(provinceId, districtId, id, pWard);
    }

    @DeleteMapping("wards/{id}")
    public ResponseEntity<Object> deleteWardById(@PathVariable int id){
        return wardService.deleteWardById(id);
    }
}
