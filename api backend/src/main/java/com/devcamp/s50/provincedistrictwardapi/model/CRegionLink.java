package com.devcamp.s50.provincedistrictwardapi.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "region_link")
public class CRegionLink {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotEmpty(message = "Tên địa điểm kết nối không được để trống")
    @Column(name = "name", length = 1000)
    private String name;

    @Column(columnDefinition = "TEXT", name = "description")
    private String description;

    @Column(name = "photo", length = 5000)
    private String photo;

    @Column(name = "address", length = 5000)
    private String address;

    @Column(name = "lat")
    private Double lat;

    @Column(name = "lng")
    private Double lng;

    @OneToMany(mappedBy = "regionLink", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<CProject> projects;
    // constructor

    public CRegionLink() {
    }

    public CRegionLink(Integer id, @NotEmpty(message = "Tên địa điểm kết nối không được để trống") String name,
            String descsription, String photo, String address, Double lat, Double lng, Set<CProject> projects) {
        this.id = id;
        this.name = name;
        this.description = descsription;
        this.photo = photo;
        this.address = address;
        this.lat = lat;
        this.lng = lng;
        this.projects = projects;
    }
    // getter and setter

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String descsription) {
        this.description = descsription;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Set<CProject> getProjects() {
        return projects;
    }

    public void setProjects(Set<CProject> projects) {
        this.projects = projects;
    }

}
