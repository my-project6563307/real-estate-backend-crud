package com.devcamp.s50.provincedistrictwardapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.provincedistrictwardapi.model.CMasterLayout;
import com.devcamp.s50.provincedistrictwardapi.service.CMasterLayoutService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CMasterLayoutController {
    @Autowired
    CMasterLayoutService masterLayoutService;  
    
    @GetMapping("master_layouts")
    public ResponseEntity<List<CMasterLayout>> getAllMasterLayout(
        @RequestParam(defaultValue = "0") String page,
        @RequestParam(defaultValue = "50") String size){
        return masterLayoutService.getAllMasterLayout(page, size);
    }

    @GetMapping("master_layouts/{id}")
    public ResponseEntity<Object> getMasterLayoutById (@PathVariable int id){
        return masterLayoutService.getMasterLayoutById(id);
    }

    @GetMapping("master_layouts/project/{projectId}")
    public ResponseEntity<Object> getMasterLayoutByprojectId (@PathVariable int projectId){
        return masterLayoutService.getMasterLayoutByprojectId(projectId);
    }

    @PostMapping("master_layouts/{projectId}")
    public ResponseEntity<Object> createMasterLayout(@PathVariable Integer projectId,@RequestBody CMasterLayout pMasterLayout){
        return masterLayoutService.createMasterLayout(projectId, pMasterLayout);
    }

    @PutMapping("master_layouts/{id}")
    public ResponseEntity<Object> updateMasterLayoutById(@RequestParam int projectId, @PathVariable int id,@RequestBody CMasterLayout pMasterLayout){
        return masterLayoutService.updateMasterLayoutById(projectId, id, pMasterLayout);
    }

    @DeleteMapping("master_layouts/{id}")
    public ResponseEntity<Object> deleteMasterLayoutById(@PathVariable int id){
        return masterLayoutService.deleteMasterLayoutById(id);
    }
}
