package com.devcamp.s50.provincedistrictwardapi.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.s50.provincedistrictwardapi.model.CMasterLayout;
import com.devcamp.s50.provincedistrictwardapi.model.CProject;
import com.devcamp.s50.provincedistrictwardapi.repository.IMasterLayoutRepository;
import com.devcamp.s50.provincedistrictwardapi.repository.IProjectRepository;

@Service
public class CMasterLayoutService {
    @Autowired
    IMasterLayoutRepository masterLayoutRepository;
    @Autowired
    IProjectRepository projectRepository;

    //Get all Master layout
    public ResponseEntity<List<CMasterLayout>> getAllMasterLayout(String page, String size){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<CMasterLayout> masterLayoutList = new ArrayList<>();
            masterLayoutRepository.findAll(pageWithElements).forEach(masterLayoutList::add);

            return new ResponseEntity<>(masterLayoutList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    //Get Master layout by Id
    public ResponseEntity<Object> getMasterLayoutById (int id){
        try {
            Optional<CMasterLayout> masterLayoutData = masterLayoutRepository.findById(id);
            if(masterLayoutData.isPresent()){

                CMasterLayout masterLayoutFound = masterLayoutData.get();

                return new ResponseEntity<>(masterLayoutFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get master layout by project id
    public ResponseEntity<Object> getMasterLayoutByprojectId (int projectId){
        try {
            Optional<CProject> projectData = projectRepository.findById(projectId);
            if(projectData.isPresent()){

                CProject projectFound = projectData.get();
                Set<CMasterLayout> masterLayoutList = projectFound.getMasterLayout();

                return new ResponseEntity<>(masterLayoutList, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Create a new master layout with projectid
    public ResponseEntity<Object> createMasterLayout(int projectId, CMasterLayout pMasterLayout){
        try {
            Optional<CProject> projectData = projectRepository.findById(projectId);
            if(projectData.isPresent()){

                CProject projectFound = projectData.get();
                CMasterLayout newMasterLayout = new CMasterLayout();

                newMasterLayout.setName(pMasterLayout.getName());
                newMasterLayout.setDescription(pMasterLayout.getDescription());
                newMasterLayout.setAcreage(pMasterLayout.getAcreage());
                newMasterLayout.setApartmentList(pMasterLayout.getApartmentList());
                newMasterLayout.setPhoto(pMasterLayout.getPhoto());
                newMasterLayout.setDate_create(new Date());
                newMasterLayout.setDate_update(new Date());
                newMasterLayout.setProject(projectFound);
    
                CMasterLayout savedCMasterLayout = masterLayoutRepository.save(newMasterLayout);
    
                return new ResponseEntity<>(savedCMasterLayout, HttpStatus.CREATED);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND); 

        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity().body("Failed to Create a specified Master Layout: " + e.getCause().getCause().getMessage());
        }
    }

    //Update master layout by id + projectId
    public ResponseEntity<Object> updateMasterLayoutById(Integer projectId, int id, CMasterLayout pMasterLayout){
        try {
            Optional<CMasterLayout> masterLayoutData = masterLayoutRepository.findById(id);
            Optional<CProject> projectData = projectRepository.findById(projectId);

            if (masterLayoutData.isPresent()){

                CMasterLayout masterLayoutFound =  masterLayoutData.get();

                masterLayoutFound.setName(pMasterLayout.getName());
                masterLayoutFound.setDescription(pMasterLayout.getDescription());
                masterLayoutFound.setAcreage(pMasterLayout.getAcreage());
                masterLayoutFound.setApartmentList(pMasterLayout.getApartmentList());
                masterLayoutFound.setPhoto(pMasterLayout.getPhoto());
                masterLayoutFound.setDate_update(new Date());
                masterLayoutFound.setProject(projectData.get());

                return new ResponseEntity<>(masterLayoutRepository.save(masterLayoutFound), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //delete design Unit by id
    public ResponseEntity<Object> deleteMasterLayoutById(int id){
        try {
            Optional<CMasterLayout> masterLayoutData = masterLayoutRepository.findById(id);
            if(masterLayoutData.isPresent()){
                masterLayoutRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
