package com.devcamp.s50.provincedistrictwardapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.provincedistrictwardapi.model.CEmployees;
import com.devcamp.s50.provincedistrictwardapi.service.CEmployeesService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CEmployeesController {
    @Autowired
    CEmployeesService employeesService;

    @GetMapping("employees")
    public ResponseEntity<List<CEmployees>> getAllEmployees(
        @RequestParam(defaultValue = "0") String page,
        @RequestParam(defaultValue = "50") String size){
        return employeesService.getAllEmployees(page, size);
    }

    @GetMapping("employees/{id}")
    public ResponseEntity<CEmployees> getDesignUnitById (@PathVariable int id){
        return employeesService.getEmployeesById(id);
    }

    @PostMapping("employees")
    public ResponseEntity<Object> createEmployees(@RequestBody CEmployees pEmployees){
        return employeesService.createEmployees(pEmployees);
    }

    @PutMapping("employees/{id}")
    public ResponseEntity<Object> updateEmployeesById(@PathVariable int id,@RequestBody CEmployees pEmployees){
        return employeesService.updateEmployeesById(id, pEmployees);
    }

    @DeleteMapping("employees/{id}")
    public ResponseEntity<Object> deleteEmployeesById(@PathVariable int id){
        return employeesService.deleteEmployeesById(id);
    }
}
