package com.devcamp.s50.provincedistrictwardapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.provincedistrictwardapi.model.CInvestor;

public interface IInvestorRepository extends JpaRepository<CInvestor, Integer>{
    
}
