package com.devcamp.s50.provincedistrictwardapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.provincedistrictwardapi.model.CMasterLayout;

public interface IMasterLayoutRepository extends JpaRepository<CMasterLayout, Integer>{
    
}
