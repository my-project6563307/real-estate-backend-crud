package com.devcamp.s50.provincedistrictwardapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.provincedistrictwardapi.model.CProject;

public interface IProjectRepository extends JpaRepository<CProject, Integer>{
    
}
