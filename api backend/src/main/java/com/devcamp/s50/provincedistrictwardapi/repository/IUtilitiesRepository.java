package com.devcamp.s50.provincedistrictwardapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.provincedistrictwardapi.model.CUtilities;

public interface IUtilitiesRepository extends JpaRepository<CUtilities, Integer>{
    
}
