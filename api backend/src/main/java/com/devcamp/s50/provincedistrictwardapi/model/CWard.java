package com.devcamp.s50.provincedistrictwardapi.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "ward")
public class CWard {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotEmpty(message = "Ward name không được để trống")
    @Column(name = "name", nullable = false)
    private String name;

    @NotEmpty(message = "Prefix không được để trống")
    @Size(min = 2, message = "Prefix phải có tối thiểu 2 kí tự")
    @Column(name = "prefix", nullable = false)
    private String prefix;

    @ManyToOne
    @JoinColumn(name = "district_id")
    private CDistrict district;

    @ManyToOne
    @JoinColumn(name = "province_id")
    private CProvince province;

    @OneToMany(mappedBy = "ward", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<CRealEstate> realestate;

    public CWard() {
    }

    public CWard(Integer id, @NotEmpty(message = "Ward name không được để trống") String name,
            @NotEmpty(message = "Prefix không được để trống") @Size(min = 2, message = "Prefix phải có tối thiểu 2 kí tự") String prefix,
            CDistrict district, CProvince province, Set<CRealEstate> realestate) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
        this.district = district;
        this.province = province;
        this.realestate = realestate;
    }

    public Integer getId() {
        return id;
    }

    public Set<CRealEstate> getRealestate() {
        return realestate;
    }

    public void setRealestate(Set<CRealEstate> realestate) {
        this.realestate = realestate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public CDistrict getDistrict() {
        return district;
    }

    public void setDistrict(CDistrict district) {
        this.district = district;
    }

    public CProvince getProvince() {
        return province;
    }

    public void setProvince(CProvince province) {
        this.province = province;
    }

}
