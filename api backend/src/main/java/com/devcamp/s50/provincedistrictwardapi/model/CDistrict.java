package com.devcamp.s50.provincedistrictwardapi.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "district")
public class CDistrict {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotEmpty(message = "District name không được để trống")
    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @NotEmpty(message = "Prefix không được để trống")
    @Size(min = 2, message = "Prefix phải có tối thiểu 2 kí tự")
    @Column(name = "prefix", nullable = false, length = 20)
    private String prefix;

    @ManyToOne
    @JoinColumn(name = "province_id")
    private CProvince province;

    @OneToMany(mappedBy = "district", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<CWard> wards;

    @OneToMany(mappedBy = "district", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<CStreet> streets;

    @OneToMany(mappedBy = "district", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<CRealEstate> realestates;

    public CDistrict() {
    }

    public CDistrict(Integer id, @NotEmpty(message = "District name không được để trống") String name,
            @NotEmpty(message = "Prefix không được để trống") @Size(min = 2, message = "Prefix phải có tối thiểu 2 kí tự") String prefix,
            CProvince province, Set<CWard> wards, Set<CStreet> streets, Set<CRealEstate> realestates) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
        this.province = province;
        this.wards = wards;
        this.streets = streets;
        this.realestates = realestates;
    }

    public Integer getId() {
        return id;
    }

    public Set<CStreet> getStreets() {
        return streets;
    }

    public void setStreets(Set<CStreet> streets) {
        this.streets = streets;
    }

    public Set<CRealEstate> getRealestates() {
        return realestates;
    }

    public void setRealestates(Set<CRealEstate> realestates) {
        this.realestates = realestates;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public CProvince getProvince() {
        return province;
    }

    public void setProvince(CProvince province) {
        this.province = province;
    }

    public Set<CWard> getWards() {
        return wards;
    }

    public void setWards(Set<CWard> wards) {
        this.wards = wards;
    }

}
