package com.devcamp.s50.provincedistrictwardapi.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "master_layout")
public class CMasterLayout {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "acreage", columnDefinition = "decimal")
    private BigDecimal acreage;

    @Column(name = "apartment_list")
    private String apartmentList;

    @Column(name = "photo")
    private String photo;

    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @Column(name = "date_create")
    @JsonFormat(pattern = "MM/dd/yy")
    private Date date_create;

    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    @Column(name = "date_update")
    @JsonFormat(pattern = "MM/dd/yy")
    private Date date_update;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private CProject project;
    // constructor

    public CMasterLayout() {
    }

    public CMasterLayout(Integer id, String name, String description, BigDecimal acreage, String apartmentList,
            String photo, Date date_create, Date date_update, CProject project) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.acreage = acreage;
        this.apartmentList = apartmentList;
        this.photo = photo;
        this.date_create = date_create;
        this.date_update = date_update;
        this.project = project;
    }
    // getter and setter

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAcreage() {
        return acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public String getApartmentList() {
        return apartmentList;
    }

    public void setApartmentList(String apartmentList) {
        this.apartmentList = apartmentList;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Date getDate_create() {
        return date_create;
    }

    public void setDate_create(Date date_create) {
        this.date_create = date_create;
    }

    public Date getDate_update() {
        return date_update;
    }

    public void setDate_update(Date date_update) {
        this.date_update = date_update;
    }

    public CProject getProject() {
        return project;
    }

    public void setProject(CProject project) {
        this.project = project;
    }

}
