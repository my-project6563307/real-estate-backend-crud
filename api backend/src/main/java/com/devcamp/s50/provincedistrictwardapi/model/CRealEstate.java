package com.devcamp.s50.provincedistrictwardapi.model;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "realestate")
public class CRealEstate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "title", length = 2000)
    private String title;

    @Column(name = "type", length = 2)
    private Integer type;

    @Column(name = "request", length = 2)
    private Integer request;

    @Column(name = "address", length = 2000)
    private String address;

    @Column(name = "price", length = 60, columnDefinition = "bigint")
    private Long price;

    @Transient
    NumberFormat nf = NumberFormat.getInstance(Locale.US);
    @Transient
    private String priceFormat;

    @Column(name = "price_min", length = 60, columnDefinition = "bigint")
    private Long priceMin;

    @Column(name = "price_time", length = 2, columnDefinition = "tinyint")
    private Short priceTime;

    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @JsonFormat(pattern = "dd-mm-yyyy")
    @Column(name = "date_create")
    private Date date_create;

    @Column(name = "acreage", length = 12, columnDefinition = "decimal")
    private BigDecimal acreage;

    @Column(name= "direction", length = 2)
    private Integer direction;

    @Column(name = "total_floors", length = 2)
    private Integer total_floors;

    @Column(name = "number_floors", length = 2)
    private Integer number_floors;

    @Column(name = "bath", length = 2)
    private Integer bath;

    @Column(name = "apart_code", length = 10)
    private String apart_code;

    @Column(name = "wall_area", length = 12, columnDefinition = "decimal")
    private BigDecimal wall_area;

    @Column(name = "bedroom", length = 4, columnDefinition = "tinyint")
    private Short bedroom;

    @Column(name = "balcony", length = 4, columnDefinition = "tinyint")
    private Short balcony;

    @Column(name = "landscape_view")
    private String landscape_view;

    @Column(name = "apart_loca", length = 4, columnDefinition = "tinyint")
    private Short apart_loca;

    @Column(name = "apart_type", length = 4, columnDefinition = "tinyint")
    private Short apart_type;

    @Column(name = "furniture_type", length = 4, columnDefinition = "tinyint")
    private Short  furniture_type;

    @Column(name = "price_rent",length = 20)
    private Short price_rent;

    @Column(name = "return_rate",length = 3)
    private Double return_rate;

    @Column(name = "legal_doc",length = 11)
    private Integer legal_doc;

    @Column(name = "description", length = 2000)
    private String description;
    
    @Column(name = "width_y",length = 6)
    private Integer width_y;

    @Column(name = "long_x",length = 6)
    private Integer long_x;

    @Column(name = "street_house", length = 1, columnDefinition = "tinyint")
    private Short street_house;
    
    @Column(name = "FSBO", length = 1, columnDefinition = "tinyint")
    private Short FSBO;

    @Column(name = "view_num")
    private Integer view_num;

    @Column(name = "create_by")
    private Integer create_by;

    @Column(name = "update_by")
    private Integer update_by;

    @Column(name = "shape", length = 200)
    private String shape;

    @Column(name = "distance2facade",length = 12)
    private Integer distance2facade;

    @Column(name = "adjacent_facade_num",length = 2)
    private Integer adjacent_facade_num;

    @Column(name = "adjacent_road", length = 200)
    private String adjacent_road;

    @Column(name = "alley_min_width",length = 6)
    private Integer alley_min_width;

    @Column(name = "adjacent_alley_min_width",length = 6)
    private Integer adjacent_alley_min_width;

    @Column(name = "factor",length = 10)
    private Integer factor;

    @Column(name = "structure", length = 2000)
    private String structure;

    @Column(name = "DTSXD",length = 6)
    private Integer DTSXD;

    @Column(name = "CLCL",length = 2)
    private Integer CLCL;

    @Column(name = "CTXD_price",length = 12)
    private Integer CTXD_price;

    @Column(name = "CTXD_value",length = 12)
    private Integer CTXD_value;

    @Column(name = "photo", length = 2000)
    private String photo;

    @Column(name = "lat")
    private Double lat;

    @Column(name = "lng")
    private Double lng;

    @ManyToOne
    @JoinColumn(name = "province_id")
    private CProvince province;

    @ManyToOne
    @JoinColumn(name = "district_id")
    private CDistrict district;

    @ManyToOne
    @JoinColumn(name = "ward_id")
    private CWard ward;

    @ManyToOne
    @JoinColumn(name = "street_id")
    private CStreet street;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private CCustomer customer;
    
    @ManyToOne
    @JoinColumn(name = "project_id")
    private CProject project;

    // constructor
    public CRealEstate() {
    }

    public CRealEstate(Integer id, String title, Integer type, Integer request, String address, Long price, Long priceMin,
            Short priceTime, Date date_create, BigDecimal acreage, Integer direction, Integer total_floors,
            Integer number_floors, Integer bath, String apart_code, BigDecimal wall_area, Short bedroom, Short balcony,
            String landscape_view, Short apart_loca, Short apart_type, Short furniture_type, Short price_rent,
            Double return_rate, Integer legal_doc, String description, Integer width_y, Integer long_x,
            Short street_house, Short fSBO, Integer view_num, Integer create_by, Integer update_by, String shape,
            Integer distance2facade, Integer adjacent_facade_num, String adjacent_road, Integer alley_min_width,
            Integer adjacent_alley_min_width, Integer factor, String structure, Integer dTSXD, Integer cLCL,
            Integer cTXD_price, Integer cTXD_value, String photo, Double lat, Double lng, CProvince province,
            CDistrict district, CWard ward, CStreet street, CCustomer customer, CProject project) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.request = request;
        this.address = address;
        this.price = price;
        this.priceMin = priceMin;
        this.priceTime = priceTime;
        this.date_create = date_create;
        this.acreage = acreage;
        this.direction = direction;
        this.total_floors = total_floors;
        this.number_floors = number_floors;
        this.bath = bath;
        this.apart_code = apart_code;
        this.wall_area = wall_area;
        this.bedroom = bedroom;
        this.balcony = balcony;
        this.landscape_view = landscape_view;
        this.apart_loca = apart_loca;
        this.apart_type = apart_type;
        this.furniture_type = furniture_type;
        this.price_rent = price_rent;
        this.return_rate = return_rate;
        this.legal_doc = legal_doc;
        this.description = description;
        this.width_y = width_y;
        this.long_x = long_x;
        this.street_house = street_house;
        FSBO = fSBO;
        this.view_num = view_num;
        this.create_by = create_by;
        this.update_by = update_by;
        this.shape = shape;
        this.distance2facade = distance2facade;
        this.adjacent_facade_num = adjacent_facade_num;
        this.adjacent_road = adjacent_road;
        this.alley_min_width = alley_min_width;
        this.adjacent_alley_min_width = adjacent_alley_min_width;
        this.factor = factor;
        this.structure = structure;
        DTSXD = dTSXD;
        CLCL = cLCL;
        CTXD_price = cTXD_price;
        CTXD_value = cTXD_value;
        this.photo = photo;
        this.lat = lat;
        this.lng = lng;
        this.province = province;
        this.district = district;
        this.ward = ward;
        this.street = street;
        this.customer = customer;
        this.project = project;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getRequest() {
        return request;
    }

    public void setRequest(Integer request) {
        this.request = request;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(Long priceMin) {
        this.priceMin = priceMin;
    }

    public Short getPriceTime() {
        return priceTime;
    }

    public void setPriceTime(Short priceTime) {
        this.priceTime = priceTime;
    }

    public Date getDate_create() {
        return date_create;
    }

    public void setDate_create(Date date_create) {
        this.date_create = date_create;
    }

    public BigDecimal getAcreage() {
        return acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

    public Integer getTotal_floors() {
        return total_floors;
    }

    public void setTotal_floors(Integer total_floors) {
        this.total_floors = total_floors;
    }

    public Integer getNumber_floors() {
        return number_floors;
    }

    public void setNumber_floors(Integer number_floors) {
        this.number_floors = number_floors;
    }

    public Integer getBath() {
        return bath;
    }

    public void setBath(Integer bath) {
        this.bath = bath;
    }

    public String getApart_code() {
        return apart_code;
    }

    public void setApart_code(String apart_code) {
        this.apart_code = apart_code;
    }

    public BigDecimal getWall_area() {
        return wall_area;
    }

    public void setWall_area(BigDecimal wall_area) {
        this.wall_area = wall_area;
    }

    public Short getBedroom() {
        return bedroom;
    }

    public void setBedroom(Short bedroom) {
        this.bedroom = bedroom;
    }

    public Short getBalcony() {
        return balcony;
    }

    public void setBalcony(Short balcony) {
        this.balcony = balcony;
    }

    public String getLandscape_view() {
        return landscape_view;
    }

    public void setLandscape_view(String landscape_view) {
        this.landscape_view = landscape_view;
    }

    public Short getApart_loca() {
        return apart_loca;
    }

    public void setApart_loca(Short apart_loca) {
        this.apart_loca = apart_loca;
    }

    public Short getApart_type() {
        return apart_type;
    }

    public void setApart_type(Short apart_type) {
        this.apart_type = apart_type;
    }

    public Short getFurniture_type() {
        return furniture_type;
    }

    public void setFurniture_type(Short furniture_type) {
        this.furniture_type = furniture_type;
    }

    public Short getPrice_rent() {
        return price_rent;
    }

    public void setPrice_rent(Short price_rent) {
        this.price_rent = price_rent;
    }

    public Double getReturn_rate() {
        return return_rate;
    }

    public void setReturn_rate(Double return_rate) {
        this.return_rate = return_rate;
    }

    public Integer getLegal_doc() {
        return legal_doc;
    }

    public void setLegal_doc(Integer legal_doc) {
        this.legal_doc = legal_doc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getWidth_y() {
        return width_y;
    }

    public void setWidth_y(Integer width_y) {
        this.width_y = width_y;
    }

    public Integer getLong_x() {
        return long_x;
    }

    public void setLong_x(Integer long_x) {
        this.long_x = long_x;
    }

    public Short getStreet_house() {
        return street_house;
    }

    public void setStreet_house(Short street_house) {
        this.street_house = street_house;
    }

    public Short getFSBO() {
        return FSBO;
    }

    public void setFSBO(Short fSBO) {
        FSBO = fSBO;
    }

    public Integer getView_num() {
        return view_num;
    }

    public void setView_num(Integer view_num) {
        this.view_num = view_num;
    }

    public Integer getCreate_by() {
        return create_by;
    }

    public void setCreate_by(Integer create_by) {
        this.create_by = create_by;
    }

    public Integer getUpdate_by() {
        return update_by;
    }

    public void setUpdate_by(Integer update_by) {
        this.update_by = update_by;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public Integer getDistance2facade() {
        return distance2facade;
    }

    public void setDistance2facade(Integer distance2facade) {
        this.distance2facade = distance2facade;
    }

    public Integer getAdjacent_facade_num() {
        return adjacent_facade_num;
    }

    public void setAdjacent_facade_num(Integer adjacent_facade_num) {
        this.adjacent_facade_num = adjacent_facade_num;
    }

    public String getAdjacent_road() {
        return adjacent_road;
    }

    public void setAdjacent_road(String adjacent_road) {
        this.adjacent_road = adjacent_road;
    }

    public Integer getAlley_min_width() {
        return alley_min_width;
    }

    public void setAlley_min_width(Integer alley_min_width) {
        this.alley_min_width = alley_min_width;
    }

    public Integer getAdjacent_alley_min_width() {
        return adjacent_alley_min_width;
    }

    public void setAdjacent_alley_min_width(Integer adjacent_alley_min_width) {
        this.adjacent_alley_min_width = adjacent_alley_min_width;
    }

    public Integer getFactor() {
        return factor;
    }

    public void setFactor(Integer factor) {
        this.factor = factor;
    }

    public String getStructure() {
        return structure;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    public Integer getDTSXD() {
        return DTSXD;
    }

    public void setDTSXD(Integer dTSXD) {
        DTSXD = dTSXD;
    }

    public Integer getCLCL() {
        return CLCL;
    }

    public void setCLCL(Integer cLCL) {
        CLCL = cLCL;
    }

    public Integer getCTXD_price() {
        return CTXD_price;
    }

    public void setCTXD_price(Integer cTXD_price) {
        CTXD_price = cTXD_price;
    }

    public Integer getCTXD_value() {
        return CTXD_value;
    }

    public void setCTXD_value(Integer cTXD_value) {
        CTXD_value = cTXD_value;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public CProvince getProvince() {
        return province;
    }

    public void setProvince(CProvince province) {
        this.province = province;
    }

    public CDistrict getDistrict() {
        return district;
    }

    public void setDistrict(CDistrict district) {
        this.district = district;
    }

    public CWard getWard() {
        return ward;
    }

    public void setWard(CWard ward) {
        this.ward = ward;
    }

    public CStreet getStreet() {
        return street;
    }

    public void setStreet(CStreet street) {
        this.street = street;
    }

    public CCustomer getCustomer() {
        return customer;
    }

    public void setCustomer(CCustomer customer) {
        this.customer = customer;
    }

    public CProject getProject() {
        return project;
    }

    public void setProject(CProject project) {
        this.project = project;
    }

    public String getPriceFormat() {
        priceFormat = nf.format(price);
        return priceFormat;
    }
}
